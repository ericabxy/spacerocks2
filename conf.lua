function love.conf(t)
  t.version = '11.3'
  if t.window then
    t.window.title = "Space Rocks 2"
    t.window.icon = "icon.png"
    t.window.width = 640
    t.window.height = 480
  end
end

-- for compatibility with LÖVE
if not lutro then
  lutro = love
  INVERT = true
  require'comp'
end
